package library;
import java.io.*;
import java.util.*;

/**
 * Класс, представляющий библиотеку книг.
 */
public class Library implements Serializable {

    private final List<Book> books = new ArrayList<>();
    private final String librarian;

    /**
     * Конструктор для создания библиотеки с указанием имени библиотекаря.
     *
     * @param librarian Имя библиотекаря.
     */
    public Library(String librarian) {

        this.librarian = librarian;

    }

    /**
     * Конструктор для создания библиотеки с указанием имени библиотекаря и загрузкой книг из файла.
     *
     * @param filename  Имя файла с информацией о книгах.
     * @param librarian Имя библиотекаря.
     */
    public Library(String filename, String librarian) {
        readBookAsText(filename);
        this.librarian = librarian;

    }

    /**
     * Добавить книгу в библиотеку.
     *
     * @param book Добавляемая книга.
     */
    public void addBook(Book book) {books.add(book);}

    /**
     * Удалить книгу из библиотеки.
     *
     * @param book Удаляемая книга.
     */
    public void removeBook(Book book) {

        for (Book temp : books) {
        if (temp.getTitle().equals(book.getTitle())) {
            books.remove(temp);
            break;
        }
    }}

    /**
     * Поиск книги по идентификатору.
     *
     * @param book_Id Идентификатор книги.
     * @return Книга, если найдена, в противном случае - null.
     */
    public Book searchByBookId(int book_Id) {
        for (Book book : books) {
            if (book_Id == book.getBookId())
            {
                return book;
            }
        }
        return null;
    }

    /**
     * Поиск книг по автору.
     *
     * @param author Автор книги.
     * @return Строка с результатами поиска.
     */
    public String searchByAuthor(String author){

        String result = "Поиск книг по автору " + author + "... \n";

        if(!books.isEmpty()) {
            for (Book book : books) {

                String authorName = author.toLowerCase();
                String bookAuthor = book.getAuthor().toLowerCase();

                int dotIndex = authorName.indexOf('.');

                // Если точка найдена И не в конце строки
                if (dotIndex >= 0 && dotIndex < authorName.length() - 1) {
                    authorName = authorName.substring(dotIndex + 1).trim();
                }

                if (bookAuthor.contains(authorName)) {
                    result = result + book + "\n";
                }
            }
        }
        else { result = result + "Нет такого автора, простите"; }

        return result;

    }
    /**
     * Поиск книг по названию.
     *
     * @param title Название книги.
     * @return Строка с результатами поиска.
     */
    public String searchByTitle(String title){

        String result = "Поиск книг по названию " + title  + "... \n";

        if(!books.isEmpty()) {
            for (Book book : books) {

                String bookTitle = title.toLowerCase();
                String bookName = book.getTitle().toLowerCase();

                if (bookName.contains(bookTitle)) {
                    result = result + book + "\n";
                }
            }
        }
        else { result = result + "Нет такой книги простите "; }

        return result;
    }


    /**
     * Получить список всех книг в библиотеке.
     *
     * @return Строка с информацией о всех книгах в библиотеке.
     */
    public String listAllBooks() {
        String allBooks = "";
        if(!books.isEmpty()) {
            for (Book book : books) { allBooks = allBooks + book.toString() + "\n"; } }
        else { allBooks = allBooks + " Сейчас библиотека пуста "; }
        return allBooks;
    }

    /**
     * Получить список всех авторов в библиотеке.
     *
     * @return Строка с именами всех авторов в библиотеке.
     */
    public String listAllAuthors() {
        String allNames = "";
        if(!books.isEmpty()) {
            for (Book book : books) { allNames = allNames + book.getAuthor() + "\n"; } }
        else { allNames = allNames + "\n" + "К сожалению, такого автора нет"; }
        return allNames;
    }

    /**
     * Сохранить состояние библиотеки в файл.
     *
     * @param filename Имя файла для сохранения.
     */
    public void saveLibrary(String filename) {
        //Сериализция
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(filename))) {
            outputStream.writeObject(this);
            System.out.println("Библиотека сохранена в " + filename + "\n" );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Восстановить состояние библиотеки из файла.
     *
     * @param filename Имя файла для восстановления.
     * @return Восстановленный объект библиотеки.
     */
    public Library restoreLibrary(String filename) {
        //Десериализация
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(filename))) {
            System.out.println("Библиотека загружена из " + filename +  "\n");
            return (Library)(inputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Загрузить книги из текстового файла.
     *
     * @param filename Имя файла для загрузки.
     */
    private void readBookAsText(String filename) {
         try (Scanner scanner = new Scanner(new File(filename))) {

             while (scanner.hasNextLine()) {

                 String[] tokens = scanner.nextLine().split(",");

                int book_Id = Integer.parseInt(tokens[0]);

                books.add(new Book(book_Id, tokens[1], tokens[2]));


            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Возвращает строковое представление информации о библиотеке, содержащий приветсвие, имя библиотекаря и список доступных книг.
     *
     * @return Строковое представление книги библиотеки.
     */
    public String toString() {
        return "  * Здраствуйте  " + librarian + "! * "+"\n" + listAllBooks() + "\n";
    }

}