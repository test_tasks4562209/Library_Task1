package library;
import java.io.Serializable;

/**
 * Класс, представляющий книгу в библиотеке.
 */
public class Book implements Serializable {

    /**
     * Уникальный идентификатор книги.
     */
    private final int book_Id;

    /**
     * Название книги.
     */
    private final String title;

    /**
     * Автор книги.
     */
    private final String author;

    /**
     * Конструктор для создания экземпляра книги.
     *
     * @param book_Id Уникальный идентификатор книги.
     * @param title   Название книги.
     * @param author  Автор книги.
     */
    public Book ( int book_Id, String title, String author )
    {
        this.book_Id = book_Id;
        this.title = title;
        this.author = author;
    }

    /**
     * Получить идентификатор книги.
     *
     * @return Идентификатор книги.
     */
    public int getBookId()
    {
        return book_Id;
    }

    /**
     * Получить название книги.
     *
     * @return Название книги.
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Получить автора книги.
     *
     * @return Автор книги.
     */
    public String getAuthor() { return author; }


    /**
     * Возвращает строковое представление информации о книге, включая уникальный идентификатор, название и автора..
     *
     * @return Строковое представление книги.
     */
    public String toString() { return "Книга ID: "   + getBookId() +  " - Название: " + getTitle() + " - Автор: " + getAuthor() + "\n" ; }}
