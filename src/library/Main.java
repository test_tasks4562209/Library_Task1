package library;


/**
 * Главный класс программы.
 */
public class Main {


    /**
     * Точка входа в программу.
     *
     * @param args Аргументы командной строки.
     */
    public static void main(String[] args) {


        //Library oldLibrary = new Library( "Алексей");
        //System.out.println(oldLibrary.listAllBooks());


        //Некоторые книги добавляются в библиотеку из текстового файла при создании объекта библиотеки
        Library myLibrary = new Library( "books.txt","Алексей");


        System.out.println("Доступные книги: "  +  "\n");
        System.out.println(myLibrary.listAllBooks());

        myLibrary.addBook(new Book(1004, "Wuthering Heights","Emily Bronte"));
        myLibrary.addBook(new Book(1005, "War and Piece", "Leo Tolstoy"));
        myLibrary.addBook(new Book(1006, "Father and Sons", "Ivan Turgenev"));
        myLibrary.addBook(new Book(1007, "Anna Karenina", "Leo Tolstoy"));
        myLibrary.addBook(new Book(1008, "The Master and Margarita", "Mikhail Bulgakov"));

        //System.out.println(myLibrary) ;

        System.out.println(myLibrary.searchByAuthor("Tolstoy"));
        System.out.println(myLibrary.searchByAuthor("Leo Tolstoy"));
        System.out.println(myLibrary.searchByAuthor("L.Tolstoy "));

        //Сериализация
        myLibrary.saveLibrary("library_books.txt");

        //Десериализация
        Library restoredLibrary = myLibrary.restoreLibrary("library_books.txt");
        System.out.println(restoredLibrary);

    }


}